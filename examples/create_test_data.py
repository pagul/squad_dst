import json
import argparse
import os


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--test_dir", default=None, type=str, required=True,
                        help="Directory with test dialogues")
    parser.add_argument("--output_dir", default=None, type=str, required=True,
                        help="Directory with test dialogues")

    args = parser.parse_args()
    entries = []
    output_file = os.path.join(args.output_dir, "test.json")
    for i in range(1, 35):
        with open(
            os.path.join(
                args.test_dir,
                "dialogues_{:03d}.json".format(i)), "r"
        ) as f:
            entries += json.load(f)

    print("Writing predictions to: %s" % output_file)
    with open(output_file, "w") as f:
        json.dump(entries, f, indent=4)


if __name__ == "__main__":
    main()
