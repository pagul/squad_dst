import os
import json
import argparse

from tqdm import tqdm
from collections import defaultdict


def write_predictions_to_test_dir(pred_dialogs, test_predictions_dir):
    dialogues_idx_to_len = {}

    for i in range(1, 35):
        dialogues_idx_to_len[i] = 128

    dialogues_idx_to_len[11] = 51
    dialogues_idx_to_len[34] = 54

    if not os.path.exists(test_predictions_dir):
        os.makedirs(test_predictions_dir)

    dialogue_num = 0
    for i in range(1, 35):
        with open(
            os.path.join(
                test_predictions_dir,
                "dialogues_{:03d}.json".format(i)), "w"
        ) as f:
            block_len = dialogues_idx_to_len[i]
            json.dump(pred_dialogs[dialogue_num: dialogue_num + block_len], f, indent=4)
            dialogue_num += block_len


def create_prediction_dict(path_to_prediction_file, path_to_intent_prediction_file):
    with open(path_to_prediction_file, 'r') as f:
        line_predictions = json.load(f)

    with open(path_to_intent_prediction_file, 'r') as f:
        line_intent_predictions = json.load(f)

    all_predictions = defaultdict(dict)

    for key, prediction in line_predictions.items():
        dialogue_id, turn_id, service, slot = key.split('-')
        [
            noncat_value_prediction,
            slot_gate_prediction,
            cat_value_prediction,
            is_categorical,
            requested_status,
            active_intent
        ] = prediction

        is_categorical = True if is_categorical == 'True' else False

        all_predictions[(dialogue_id, turn_id, service)].update({
            slot: {
                'value': cat_value_prediction if is_categorical else noncat_value_prediction,
                'slot_status': slot_gate_prediction,
                'requested_status': requested_status,
            },
        })

    for key, prediction in line_intent_predictions.items():
        dialogue_id, turn_id, service = key.split('-')
        all_predictions[(dialogue_id, turn_id, service)]['active_intent'] = prediction

    return all_predictions


def get_predicted_dialogue(dialogue, all_predictions):
    dialogue_id = dialogue["dialogue_id"]
    all_slot_values = defaultdict(dict)

    for turn_idx, turn in enumerate(dialogue["turns"]):
        if turn["speaker"] == "USER":
            turn_id = "{:02d}".format(turn_idx)
            for frame in turn["frames"]:
                predictions = all_predictions[(dialogue_id, turn_id, frame["service"])]

                slot_values = all_slot_values[frame["service"]]

                # Remove the slot spans and state if present.
                frame.pop("slots", None)
                frame.pop("state", None)

                # The baseline model doesn't predict slot spans. Only state predictions
                # are added.

                slot_values_update = {}
                requested_slots = []
                active_intent = ""

                for key, prediction in predictions.items():
                    if key == "active_intent":
                        active_intent = prediction
                        continue

                    if prediction['requested_status'] == "requested":
                        requested_slots.append(key)

                    if prediction['slot_status'] == "none":
                        continue
                    elif prediction['slot_status'] == "dontcare":
                        slot_values_update.update({
                            key: "dontcare"
                        })
                    else:
                        slot_values_update.update({
                            key: prediction['value']
                        })

                slot_values.update(slot_values_update)
                state = {}
                state["active_intent"] = active_intent
                state["requested_slots"] = requested_slots
                state["slot_values"] = {s: [v] for s, v in slot_values.items()}
                frame["state"] = state
    return dialogue


def write_predictions_to_file(path_to_prediction_file, path_to_intent_prediction_file,
                              path_to_dev_dialogues, output_dir, output_filename, do_test,
                              suffix=""):
    """Write the predicted dialogues as json files."""

    output_file_path = os.path.join(output_dir, output_filename)
    print("Writing predictions to %s." % output_file_path)

    all_predictions = create_prediction_dict(path_to_prediction_file, path_to_intent_prediction_file)
    with open(path_to_dev_dialogues, 'r') as f:
        dev_dialogues = json.load(f)

    pred_dialogs = []

    for dialogue in tqdm(dev_dialogues):
        pred_dialogs.append(get_predicted_dialogue(dialogue, all_predictions))

    with open(output_file_path, "w") as f:
        json.dump(pred_dialogs, f, indent=2, separators=(",", ": "), sort_keys=True)

    if do_test:
        test_predictions_dir = os.path.join(output_dir, "predictions_" + suffix)
        print("Writing test predictions to " + test_predictions_dir)
        write_predictions_to_test_dir(pred_dialogs, test_predictions_dir)


def main():
    parser = argparse.ArgumentParser()

    # Required parameters
    parser.add_argument("--dev_file", default=None, type=str, required=True,
                        help="DSTC8-style json with dev dialogues. E.g., single-domain-dev.json")
    parser.add_argument("--predict_file", default=None, type=str, required=True,
                        help="SQuAD json with predictions. E.g., predictions_.json")
    parser.add_argument("--predict_intent_file", default=None, type=str, required=True,
                        help="SQuAD json with predictions. E.g., predictions_intent_.json")
    parser.add_argument("--output_dir", default=None, type=str, required=True,
                        help="The output directory where the model checkpoints and predictions were by SQuAD written.")

    # Other parameters
    parser.add_argument("--output_filename", default='predicted_dialogues.json', type=str, required=False,
                        help="The name of file where predicted dialogues will be written.")
    parser.add_argument("--do_test", action='store_true',
                        help="Whether to run test on the test set.")
    parser.add_argument("--pred_dir_suffix", default='', type=str, required=False,
                        help="Suffix for prediction directory.")

    args = parser.parse_args()
    write_predictions_to_file(
        args.predict_file,
        args.predict_intent_file,
        args.dev_file,
        args.output_dir,
        args.output_filename,
        args.do_test,
        args.pred_dir_suffix
    )


if __name__ == "__main__":
    main()
