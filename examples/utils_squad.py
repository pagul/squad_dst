
# coding=utf-8
# Copyright 2018 The Google AI Language Team Authors and The HuggingFace Inc. team.
# Copyright (c) 2018, NVIDIA CORPORATION.  All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
""" Load SQuAD dataset. """

from __future__ import absolute_import, division, print_function

import json
import logging
import random
import math
import copy
import collections
import numpy as np

from io import open
from tqdm import tqdm

from pytorch_transformers.tokenization_bert import BasicTokenizer, whitespace_tokenize

# Required by XLNet evaluation method to compute optimal threshold (see write_predictions_extended() method)
from utils_squad_evaluate import find_all_best_thresh_v2, make_qid_to_has_ans, get_raw_scores

logger = logging.getLogger(__name__)

SLOT_STATUS_DICT = {
    "none": 0,
    "ptr": 1,
    "dontcare": 2
}

REQUESTED_DICT = {
    "not_requested": 0,
    "requested": 1
}

IDX_TO_SLOT_STATUS_DICT = {
    0: "none",
    1: "ptr",
    2: "dontcare"
}

IDX_TO_REQUESTED_STATUS_DICT = {
    0: "not_requested",
    1: "requested"
}

MAX_NUM_CAT_SLOT_VALUES = 11
MAX_NUM_INTENTS = 4


class ServiceSchema(object):
    """A wrapper for schema for a service."""

    def __init__(self, schema_json, service_id=None):
        self._service_name = schema_json["service_name"]
        self._description = schema_json["description"]
        self._schema_json = schema_json
        self._service_id = service_id

        # Construct the vocabulary for intents, slots, categorical slots,
        # non-categorical slots and categorical slot values. These vocabs are used
        # for generating indices for their embedding matrix.
        self._intents = sorted(i["description"] for i in schema_json["intents"])
        self._intent_names = sorted(i["name"] for i in schema_json["intents"])
        self.intent_descriptions = {s["name"]: s["description"] for s in schema_json["intents"]}
        self._slots = sorted(s["name"] for s in schema_json["slots"])
        self._categorical_slots = sorted(
                s["name"]
                for s in schema_json["slots"]
                if s["is_categorical"])
        # self._categorical_slots = sorted(
        #     s["name"]
        #     for s in schema_json["slots"]
        #     if s["is_categorical"] and s["name"] in self.state_slots)
        self._non_categorical_slots = sorted(
            s["name"]
            for s in schema_json["slots"]
            if not s["is_categorical"] and s["name"])
        # self._non_categorical_slots = sorted(
        #     s["name"]
        #     for s in schema_json["slots"]
        #     if not s["is_categorical"] and s["name"] in self.state_slots)
        slot_schemas = {s["name"]: s for s in schema_json["slots"]}
        self.slot_descriptions = {s["name"]: s["description"] for s in schema_json["slots"]}
        categorical_slot_values = {}
        categorical_slot_value_ids = {}
        for slot in self._categorical_slots:
            slot_schema = slot_schemas[slot]
            values = sorted(slot_schema["possible_values"])
            categorical_slot_values[slot] = values
            value_ids = {value: idx for idx, value in enumerate(values)}
            categorical_slot_value_ids[slot] = value_ids
        self._categorical_slot_values = categorical_slot_values
        self._categorical_slot_value_ids = categorical_slot_value_ids
        self._intents_to_ids = {intent: idx for idx, intent in enumerate(self._intents)}

    @property
    def schema_json(self):
        return self._schema_json

    @property
    def state_slots(self):
        """Set of slots which are permitted to be in the dialogue state."""
        state_slots = set()
        for intent in self._schema_json["intents"]:
            state_slots.update(intent["required_slots"])
            state_slots.update(intent["optional_slots"])
        return state_slots

    @property
    def service_name(self):
        return self._service_name

    @property
    def service_id(self):
        return self._service_id

    @property
    def description(self):
        return self._description

    @property
    def slots(self):
        return self._slots

    @property
    def intents(self):
        return self._intents

    @property
    def categorical_slots(self):
        return self._categorical_slots

    @property
    def non_categorical_slots(self):
        return self._non_categorical_slots

    def get_categorical_slot_values(self, slot):
        return self._categorical_slot_values[slot]

    def get_slot_from_id(self, slot_id):
        return self._slots[slot_id]

    def get_intent_from_id(self, intent_id):
        return self._intents[intent_id]

    def get_categorical_slot_from_id(self, slot_id):
        return self._categorical_slots[slot_id]

    def get_non_categorical_slot_from_id(self, slot_id):
        return self._non_categorical_slots[slot_id]

    def get_categorical_slot_value_from_id(self, slot_id, value_id):
        slot = self.categorical_slots[slot_id]
        return self._categorical_slot_values[slot][value_id]

    def get_categorical_slot_value_id(self, slot, value):
        return self._categorical_slot_value_ids[slot][value]

    def get_slot_description(self, slot):
        return self.slot_descriptions[slot]

    def get_intent_description(self, intent):
        if (intent == 'NONE'):
            return intent
        else:
            return self.intent_descriptions[intent]

class Schema(object):
    """Wrapper for schemas for all services in a dataset."""

    def __init__(self, schema_json_path):
        # Load the schema from the json file.
        with open(schema_json_path, "r", encoding='utf-8') as f:
            schemas = json.load(f)
        self._services = sorted(schema["service_name"] for schema in schemas)
        self._services_vocab = {v: k for k, v in enumerate(self._services)}
        service_schemas = {}
        for schema in schemas:
            service = schema["service_name"]
            service_schemas[service] = ServiceSchema(
                schema, service_id=self.get_service_id(service))
        self._service_schemas = service_schemas

    def get_service_id(self, service):
        return self._services_vocab[service]

    def get_service_from_id(self, service_id):
        return self._services[service_id]

    def get_service_schema(self, service):
        return self._service_schemas[service]

    @property
    def services(self):
        return self._services

class SquadExample(object):
    """
    A single training/test example for the Squad dataset.
    For examples without an answer, the start and end position are -1.
    """

    def __init__(self,
                 qas_id,
                 question_text,
                 doc_tokens,
                 orig_answer_text=None,
                 start_position=None,
                 end_position=None,
                 is_impossible=None,
                 turn_id=None,
                 slot_name=None,
                 service_name=None,
                 slot_status=None,
                 is_categorical=None,
                 possible_values=None,
                 value_id=-1,
                 possible_value=None,
                 is_requested=None,
                 service_intents=None,
                 active_intent=None,
                 active_intent_idx=None,
                 service_intent_names=None):
        self.qas_id = qas_id
        self.question_text = question_text
        self.doc_tokens = doc_tokens
        self.orig_answer_text = orig_answer_text
        self.start_position = start_position
        self.end_position = end_position
        self.is_impossible = is_impossible
        self.turn_id = turn_id
        self.slot_name = slot_name
        self.service_name = service_name
        self.slot_status = slot_status
        self.is_categorical = is_categorical
        self.possible_values = possible_values
        self.value_id = value_id
        self.possible_value = possible_value
        self.is_requested = is_requested
        self.service_intents = service_intents
        self.service_intent_names = service_intent_names
        self.active_intent = active_intent
        self.active_intent_idx = active_intent_idx

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        s = ""
        s += "qas_id: %s" % (self.qas_id)
        s += ", question_text: %s" % (
            self.question_text)
        s += ", doc_tokens: [%s]" % (" ".join(self.doc_tokens))
        if self.start_position:
            s += ", start_position: %d" % (self.start_position)
        if self.end_position:
            s += ", end_position: %d" % (self.end_position)
        if self.is_impossible:
            s += ", is_impossible: %r" % (self.is_impossible)
        if self.turn_id:
            s += ", turn_id: %s" % (self.turn_id)
        if self.slot_name:
            s += ", slot_name: %s" % (self.slot_name)
        if self.slot_name:
            s += ", slot_name: %s" % (self.service_name)
        return s


class InputFeatures(object):
    """A single set of features of data."""

    def __init__(self,
                 unique_id,
                 example_index,
                 doc_span_index,
                 tokens,
                 token_to_orig_map,
                 token_is_max_context,
                 input_ids,
                 input_mask,
                 segment_ids,
                 cls_index,
                 p_mask,
                 paragraph_len,
                 start_position=None,
                 end_position=None,
                 is_impossible=None,
                 turn_id=None,
                 slot_name=None,
                 service_name=None,
                 slot_status=None,
                 is_categorical=False,
                 possible_values=None,
                 value_id=None,
                 slot_mask=None,
                 is_requested=None,
                 pv_mask=None,
                 pv_true_val=None,
                 service_intents=None,
                 active_intent=None,
                 active_intent_idx=None,
                 intent_mask=None,
                 int_token_mask=None,
                 service_intent_names=None
                 ):
        self.unique_id = unique_id
        self.example_index = example_index
        self.doc_span_index = doc_span_index
        self.tokens = tokens
        self.token_to_orig_map = token_to_orig_map
        self.token_is_max_context = token_is_max_context
        self.input_ids = input_ids
        self.input_mask = input_mask
        self.segment_ids = segment_ids
        self.cls_index = cls_index
        self.p_mask = p_mask
        self.paragraph_len = paragraph_len
        self.start_position = start_position
        self.end_position = end_position
        self.is_impossible = is_impossible
        self.turn_id = turn_id
        self.slot_name = slot_name
        self.service_name = service_name
        self.slot_status = slot_status
        self.is_categorical = is_categorical
        self.possible_values = possible_values
        self.value_id = value_id
        self.slot_mask = slot_mask
        self.is_requested = is_requested
        self.pv_mask = pv_mask
        self.pv_true_val = pv_true_val
        self.service_intents = service_intents
        self.service_intent_names = service_intent_names
        self.active_intent = active_intent
        self.active_intent_idx = active_intent_idx
        self.intent_mask = intent_mask
        self.int_token_mask = int_token_mask


def get_state_update(current_state, prev_state):
    state_update = dict(current_state)
    for slot, values in current_state.items():
        if slot in prev_state and prev_state[slot][0] in values:
            # Remove the slot from state if its value didn't change.
            state_update.pop(slot)
    return state_update

def read_schema(input_file):
    return Schema(input_file)

def is_whitespace(c):
    if c == " " or c == "\t" or c == "\r" or c == "\n" or ord(c) == 0x202F:
        return True
    return False

def addContext(context):
    doc_tokens=[]
    char_to_word_offset=[]
    prev_is_whitespace=True
    for c in context:
        if is_whitespace(c):
            prev_is_whitespace = True
        else:
            if prev_is_whitespace:
                doc_tokens.append(c)
            else:
                doc_tokens[-1] += c
            prev_is_whitespace = False
        char_to_word_offset.append(len(doc_tokens) - 1)
    return doc_tokens, char_to_word_offset, prev_is_whitespace

def addPosValsToContext(old_doc_tokens, old_char_to_word_offset, old_prev_is_whitespace,
                        possible_values, right_answer):
    doc_tokens = copy.copy(old_doc_tokens)
    char_to_word_offset = copy.copy(old_char_to_word_offset)
    prev_is_whitespace = copy.copy(old_prev_is_whitespace)
    start_ans = -1
    end_ans = -1
    ans_text = ""
    for pv in possible_values:
        if pv in right_answer:
            start_ans = len(doc_tokens) + 1
            end_ans = start_ans + len(pv.split()) - 1
            ans_text = pv
        for c in ' [PV] ' + pv:
            if is_whitespace(c):
                prev_is_whitespace = True
            else:
                if prev_is_whitespace:
                    doc_tokens.append(c)
                else:
                    doc_tokens[-1] += c
                prev_is_whitespace = False
            char_to_word_offset.append(len(doc_tokens) - 1)
    return doc_tokens, start_ans, end_ans, ans_text


def shuffle_lists_in_proportion(list_one, list_two, block_len):
    list_one = list_one[:len(list_one) - len(list_one) % block_len]
    list_two = list_two[:len(list_two) - len(list_two) % block_len]

    long_list = list_one if len(list_one) >= len(list_two) else list_two
    short_list = list_one if len(list_one) < len(list_two) else list_two

    if not long_list:
        return short_list
    if not short_list:
        return long_list

    proportion = int(len(long_list) / len(short_list))
    new_list = []
    i, j = 0, 0

    while i < len(long_list) and j < len(short_list):
        new_list.extend(short_list[j: j + block_len])
        new_list.extend(long_list[i: i + proportion * block_len])
        i += proportion * block_len
        j += block_len

    new_list.extend(long_list[i:])
    new_list.extend(short_list[j:])
    return new_list


class SampleStatistics(object):
    def __init__(self):
        self.sampled_cat_slots = {
            'negative': 0,
            'positive': 0
        }

        self.sampled_noncat_slots = {
            'negative': 0,
            'positive': 0
        }

    def update_counts(self, is_categorical, negative):
        if is_categorical:
            self._increment_counters(self.sampled_cat_slots, negative)
        else:
            self._increment_counters(self.sampled_noncat_slots, negative)

    def print_statistics(self):
        print('Sampled categorical slots: \npositive = {}, negative = {}'.format(
            self.sampled_cat_slots['positive'], self.sampled_cat_slots['negative'])
        )

        print('Sampled non-categorical slots: \npositive = {}, negative = {}'.format(
            self.sampled_noncat_slots['positive'], self.sampled_noncat_slots['negative'])
        )

        print('Total number of slots: {}'.format(
            self.sampled_cat_slots['positive'] +
            self.sampled_cat_slots['negative'] +
            self.sampled_noncat_slots['positive'] +
            self.sampled_noncat_slots['negative']
        ))

    def _increment_counters(self, count_dict, negative):
            if negative:
                count_dict['negative'] += 1
            else:
                count_dict['positive'] += 1


# negative sampling of slots
def take_slot(slot, state_update, requested_slots, sample_prob, is_categorical, stats):
    if slot not in state_update and slot not in requested_slots:
        if np.random.uniform() > sample_prob:
            return False
        else:
            stats.update_counts(is_categorical=is_categorical, negative=True)
            return True

    stats.update_counts(is_categorical=is_categorical, negative=False)
    return True


change_speaker_token = '[unused0] '        
def read_squad_examples(args, input_file, is_training, version_2_with_negative, schema, dataset):
    """Read a SQuAD json file into a list of SquadExample."""
    with open(input_file, "r", encoding='utf-8') as reader:
        # input_data = json.load(reader)["data"]
        input_data = json.load(reader)

    ans_id = 0

    categorical_examples, non_categorical_examples = [], []
    stats = SampleStatistics()

    for dialog in input_data:
        dialog_id = dialog["dialogue_id"]
        prev_states = {}
        for turn_idx, turn in enumerate(dialog["turns"]):
            if turn["speaker"] == "USER":
                user_utterance = turn["utterance"]
                user_frames = {f["service"]: f for f in turn["frames"]}
                if turn_idx > 0:
                    system_turn = dialog["turns"][turn_idx - 1]
                    system_utterance = system_turn["utterance"] + " "
                    system_frames = {f["service"]: f for f in system_turn["frames"]}
                else:
                    system_utterance = ""
                    system_frames = {}
            else:
                continue
            turn_id = "{}-{}-{:02d}".format(dataset, dialog_id, turn_idx)
            context = system_utterance + change_speaker_token + user_utterance
            doc_tokens, char_to_word_offset, prev_is_whitespace = addContext(context)
            states = {}
            for service, user_frame in user_frames.items():

                cur_schema = schema.get_service_schema(service)
                system_frame = system_frames.get(service, None)
                state = user_frame["state"]["slot_values"]
                requested_slots = user_frame["state"]["requested_slots"]

                service_intents = cur_schema._intents
                service_intent_names = cur_schema._intent_names
                active_intent = user_frame["state"]["active_intent"]

                if active_intent in cur_schema._intents_to_ids:
                    active_intent_idx = cur_schema._intents_to_ids[active_intent]
                else:
                    active_intent_idx = MAX_NUM_INTENTS

                state_update = get_state_update(state, prev_states.get(service, {}))
                states[service] = state
                categorical_slots = cur_schema.categorical_slots
                sys_actions_values = {act['slot']: act for act in system_frame['actions'] if ('slot' in act and 'values' in act and len(act['values']) > 0)} if system_frame is not None and len(system_frame['actions']) > 0 else {}
                actions_slot_names = [slot for slot in sys_actions_values.keys()]
                user_slot_names = [slot["slot"] for slot in user_frame["slots"]]
                sys_slot_names = [slot["slot"] for slot in system_frame["slots"]] if system_frame is not None else []
                slot_names = []
                slot_names.extend(actions_slot_names)
                slot_names.extend(user_slot_names)
                slot_names.extend(sys_slot_names)
                slot_names = list(set(slot_names))

                for cat_slot in cur_schema.categorical_slots:
                    if is_training:

                        if not take_slot(cat_slot, state_update, requested_slots, sample_prob=args.cat_neg_sampling_prob, is_categorical=True, stats=stats):
                            continue

                    is_requested = REQUESTED_DICT['requested'] if cat_slot in requested_slots else REQUESTED_DICT['not_requested']

                    possible_values = cur_schema.get_categorical_slot_values(cat_slot)
                    # make new context for cat slots
                    question_text = cur_schema.get_slot_description(cat_slot) + '. ' + cur_schema.description
                    right_answer = state_update[cat_slot] if cat_slot in state_update else []
                    # new_doc_tokens, start_position, end_position, orig_answer_text = addPosValsToContext(
                    #                                                             doc_tokens,
                    #                                                             char_to_word_offset,
                    #                                                             prev_is_whitespace,
                    #                                                             possible_values=possible_values,
                    #                                                             right_answer=right_answer)

                    start_position, end_position, orig_answer_text = -1, -1, ""
                    if len(question_text) > args.max_query_length:
                        question_text = question_text[:args.max_query_length]
                    # if len(doc_tokens) > args.max_seq_length:
                    #     start_position -= (len(doc_tokens) - args.max_seq_length)
                    #     end_position -= (len(doc_tokens) - args.max_seq_length)
                    #     doc_tokens = doc_tokens[-args.max_seq_length:]

                    if cat_slot in state_update:
                        slot_status = SLOT_STATUS_DICT['dontcare'] if state_update[cat_slot] == 'dontcare' else SLOT_STATUS_DICT['ptr']
                        if len(right_answer) != 1:
                            raise Exception("more than one right answer")
                        if right_answer[0] in cur_schema._categorical_slot_value_ids[cat_slot]:
                            value_id = cur_schema._categorical_slot_value_ids[cat_slot][right_answer[0]]
                        else:
                            value_id = MAX_NUM_CAT_SLOT_VALUES
                    else:
                        slot_status = SLOT_STATUS_DICT['none']
                        if (cat_slot in sys_actions_values):
                            right_answer = sys_actions_values[cat_slot]['values'][0]
                            if right_answer[0] in cur_schema._categorical_slot_value_ids[cat_slot]:
                                value_id = cur_schema._categorical_slot_value_ids[cat_slot][right_answer[0]]
                            else:
                                value_id = MAX_NUM_CAT_SLOT_VALUES
                        else:
                            value_id = MAX_NUM_CAT_SLOT_VALUES


                    example = SquadExample(
                        qas_id=str(ans_id),
                        question_text=question_text,
                        doc_tokens=doc_tokens,
                        orig_answer_text=orig_answer_text,
                        start_position=start_position,
                        end_position=end_position,
                        is_impossible=(len(right_answer) == 0),
                        turn_id=turn_id,
                        service_name=service,
                        is_categorical=True,
                        slot_name=cat_slot,
                        slot_status=slot_status,
                        possible_values=possible_values,
                        value_id=value_id,#true value of the slot (in possible_values)
                        is_requested=is_requested,
                        service_intents=service_intents,
                        service_intent_names=service_intent_names,
                        active_intent=cur_schema.get_intent_description(active_intent),
                        active_intent_idx=active_intent_idx
                    )

                    ans_id += 1
                    categorical_examples.append(example)

                noncategorical_slots = cur_schema.non_categorical_slots
                # slot name : {slot_name, start, exclusive_end} (values)
                user_slots = {
                    slot['slot']: slot for slot in user_frame['slots']
                } if len(user_frame['slots']) > 0 else {}

                sys_slots = {
                    slot['slot']: slot for slot in system_frame['slots']
                } if system_frame is not None and len(system_frame['slots']) > 0 else {}
                sys_actions = {
                    act['act']: act for act in system_frame['actions']
                } if system_frame is not None and len(system_frame['actions']) > 0 else {}

                for noncat_slot in noncategorical_slots:

                    if is_training:
                        if not take_slot(
                                noncat_slot,
                                state_update,
                                requested_slots,
                                sample_prob=args.noncat_neg_sampling_prob,
                                is_categorical=False,
                                stats=stats
                        ):
                            continue

                    is_requested = REQUESTED_DICT['requested'] if noncat_slot in requested_slots else REQUESTED_DICT['not_requested']

                    question_text = cur_schema.get_slot_description(noncat_slot) + '. ' + cur_schema.description
                    if len(question_text) > args.max_query_length:
                        question_text = question_text[:args.max_query_length]

                    # может быть такое, что система предлагает одно, а юзер отвечает, что ему нужно другое. Пользователь важнее.
                    # проверить гипотезу, где выделенный слот не совпадает с тем, что пошло в state
                    # проверить гипотезу, если пользователь перечисляет например несколько кухонь и только одна пойдёт в ответ
                    start_position = -1
                    end_position = -1
                    if noncat_slot in user_slots:
                        start_position, end_position, orig_answer_text = get_noncat_slot_info(noncat_slot,
                                                                                              char_to_word_offset,
                                                                                              user_slots,
                                                                                              user_utterance,
                                                                                              len(system_utterance) + len(change_speaker_token))
                    elif noncat_slot in sys_slots:
                        start_position, end_position, orig_answer_text = get_noncat_slot_info(noncat_slot,
                                                                                              char_to_word_offset,
                                                                                              sys_slots,
                                                                                              system_utterance, 0)
                    elif noncat_slot in sys_actions_values:
                        start_position, end_position, orig_answer_text = get_noncat_slot_info_from_action(noncat_slot,
                                                                                                          char_to_word_offset,
                                                                                                          sys_actions_values,
                                                                                                          system_utterance, 0)

                    # TODO заменить длину токенов на длину токенов + длина question_text
                    if len(doc_tokens) > args.max_seq_length:
                        start_position -= (len(doc_tokens)-args.max_seq_length)
                        end_position -= (len(doc_tokens)-args.max_seq_length)
                        doc_tokens = doc_tokens[-args.max_seq_length:]

                    if (start_position < -1) or (end_position < -1) or (start_position >= len(doc_tokens)) or (end_position >= len(doc_tokens)):
                        continue

                    if noncat_slot in state_update:
                        is_impossible = False
                        slot_status = SLOT_STATUS_DICT['dontcare'] if state_update[noncat_slot] == 'dontcare' else SLOT_STATUS_DICT['ptr']
                    else:
                        slot_status = SLOT_STATUS_DICT['none']
                        if (start_position >= 0) and (end_position >= 0):
                            is_impossible = False
                        else:
                            is_impossible = True
                            start_position = -1
                            end_position = -1
                            orig_answer_text = ""

                    example = SquadExample(
                        qas_id=str(ans_id),
                        question_text=question_text, # slot desc
                        doc_tokens=doc_tokens, # list of history tokens
                        orig_answer_text=orig_answer_text,
                        start_position=start_position,
                        end_position=end_position,
                        is_impossible=is_impossible,
                        turn_id=turn_id,
                        slot_name=noncat_slot,
                        service_name=service,
                        slot_status=slot_status,
                        is_categorical=False,
                        value_id=MAX_NUM_CAT_SLOT_VALUES,
                        is_requested=is_requested,
                        service_intents=service_intents,
                        service_intent_names=service_intent_names,
                        active_intent=cur_schema.get_intent_description(active_intent),
                        active_intent_idx=active_intent_idx
                    )
                    ans_id += 1
                    non_categorical_examples.append(example)

            prev_states = states

    if is_training:
        random.shuffle(non_categorical_examples)
        random.shuffle(categorical_examples)

    stats.print_statistics()
    return non_categorical_examples, categorical_examples


def read_squad_examples_test(args, input_file, schema, dataset):
    with open(input_file, "r", encoding='utf-8') as reader:
        input_data = json.load(reader)

    ans_id = 0
    categorical_examples, non_categorical_examples = [], []

    for dialog in input_data:
        dialog_id = dialog["dialogue_id"]

        for turn_idx, turn in enumerate(dialog["turns"]):
            if turn["speaker"] == "USER":
                user_utterance = turn["utterance"]
                user_frames = {f["service"]: f for f in turn["frames"]}
                if turn_idx > 0:
                    system_turn = dialog["turns"][turn_idx - 1]
                    system_utterance = system_turn["utterance"] + " "
                else:
                    system_utterance = ""
            else:
                continue
            turn_id = "{}-{}-{:02d}".format(dataset, dialog_id, turn_idx)
            context = system_utterance + change_speaker_token + user_utterance
            doc_tokens, char_to_word_offset, prev_is_whitespace = addContext(context)

            for service, user_frame in user_frames.items():
                cur_schema = schema.get_service_schema(service)
                service_intents = cur_schema._intents
                service_intent_names = cur_schema._intent_names

                for cat_slot in cur_schema.categorical_slots:
                    possible_values = cur_schema.get_categorical_slot_values(cat_slot)
                    question_text = cur_schema.get_slot_description(cat_slot) + '. ' + cur_schema.description

                    start_position, end_position, orig_answer_text = -1, -1, ""
                    if len(question_text) > args.max_query_length:
                        question_text = question_text[:args.max_query_length]

                    example = SquadExample(
                        qas_id=str(ans_id),
                        question_text=question_text,
                        doc_tokens=doc_tokens,
                        orig_answer_text=orig_answer_text,
                        start_position=start_position,
                        end_position=end_position,
                        is_impossible=False,
                        turn_id=turn_id,
                        service_name=service,
                        is_categorical=True,
                        slot_name=cat_slot,
                        slot_status="none",
                        possible_values=possible_values,
                        value_id=MAX_NUM_CAT_SLOT_VALUES,
                        is_requested=None,
                        service_intents=service_intents,
                        service_intent_names=service_intent_names,
                    )

                    ans_id += 1
                    categorical_examples.append(example)

                for noncat_slot in cur_schema.non_categorical_slots:
                    question_text = cur_schema.get_slot_description(noncat_slot) + '. ' + cur_schema.description
                    if len(question_text) > args.max_query_length:
                        question_text = question_text[:args.max_query_length]

                    start_position = -1
                    end_position = -1

                    if len(doc_tokens) > args.max_seq_length:
                        start_position -= (len(doc_tokens)-args.max_seq_length)
                        end_position -= (len(doc_tokens)-args.max_seq_length)
                        doc_tokens = doc_tokens[-args.max_seq_length:]

                    example = SquadExample(
                        qas_id=str(ans_id),
                        question_text=question_text,
                        doc_tokens=doc_tokens,
                        orig_answer_text=orig_answer_text,
                        start_position=start_position,
                        end_position=end_position,
                        is_impossible=False,
                        turn_id=turn_id,
                        slot_name=noncat_slot,
                        service_name=service,
                        slot_status="none",
                        is_categorical=False,
                        value_id=MAX_NUM_CAT_SLOT_VALUES,
                        is_requested=None,
                        service_intents=service_intents,
                        service_intent_names=service_intent_names,
                    )
                    ans_id += 1
                    non_categorical_examples.append(example)

    return non_categorical_examples, categorical_examples

def get_noncat_slot_info(noncat_slot, char_to_word_offset, frame, utterance, bias):
    if noncat_slot in frame:
        start_position = char_to_word_offset[frame[noncat_slot]['start'] + bias]
        end_position = char_to_word_offset[frame[noncat_slot]['exclusive_end'] + bias - 1]
        orig_answer_text = utterance[frame[noncat_slot]['start']:frame[noncat_slot]['exclusive_end']]

    return start_position, end_position, orig_answer_text

def get_noncat_slot_info_from_action(noncat_slot, char_to_word_offset, frame, utterance, bias):
    # frame[noncat_slot] - конкретный action для слота
    start_char = utterance.find(frame[noncat_slot]['values'][0])
    start_position = char_to_word_offset[start_char + bias]
    end_char = start_char + len(frame[noncat_slot]['values'][0])
    end_position = char_to_word_offset[end_char + bias]
    orig_answer_text = frame[noncat_slot]['values'][0]
    return start_position, end_position, orig_answer_text

def _find_subword_indices(state_update, utterance, frame,
                          alignments, subwords, bias):
    """Find indices for subwords corresponding to slot values."""
    span_boundaries = {}
    for slot, values in state_update.items():
        # Get all values present in the utterance for the specified slot.
        value_char_spans = {}
        for slot_span in frame:
            # slot_span["slot"] - name of slot
            if slot_span["slot"] == slot:
                value = utterance[slot_span["start"]:slot_span["exclusive_end"]]
                start_tok_idx = slot_span["start"]
                end_tok_idx = slot_span["exclusive_end"] - 1
                if 0 <= start_tok_idx < len(subwords):
                    end_tok_idx = min(end_tok_idx, len(subwords) - 1)
                    value_char_spans[value] = (start_tok_idx + bias, end_tok_idx + bias)
        for v in values:
            if v in value_char_spans:
                span_boundaries[slot] = value_char_spans[v]
                break
    return span_boundaries

special_task_tokens = ['[unused0]']
def convert_examples_to_features(examples, tokenizer, max_seq_length,
                                 doc_stride, max_query_length, is_training,
                                 cls_token_at_end=False,
                                 cls_token='[CLS]', sep_token='[SEP]', pad_token=0,
                                 sequence_a_segment_id=0, sequence_b_segment_id=1,
                                 cls_token_segment_id=0, pad_token_segment_id=0,
                                 mask_padding_with_zero=True,
                                 max_hist_len=250, pv_token='[unused1]',
                                 pv_token_segment_id=0,
                                 int_token='[unused3]', max_intent_length=50):
    """Loads a data file into a list of `InputBatch`s."""

    unique_id = 1000000000
    # cnt_pos, cnt_neg = 0, 0
    # max_N, max_M = 1024, 1024
    # f = np.zeros((max_N, max_M), dtype=np.float32)

    features = []
    for (example_index, example) in enumerate(examples):

        # if example_index % 100 == 0:
        #     logger.info('Converting %s/%s pos %s neg %s', example_index, len(examples), cnt_pos, cnt_neg)

        slot_mask = [0] * (MAX_NUM_CAT_SLOT_VALUES + 1)
        #create mask for cat slots
        if example.is_categorical:
            # to allow model predict "none"
            slot_mask[0: len(example.possible_values) + 1] = [1] * (len(example.possible_values) + 1)
            # slot_mask[MAX_NUM_CAT_SLOT_VALUES] = 1

        intent_mask = [0] * (MAX_NUM_INTENTS + 1)
        intent_mask[0: len(example.service_intents) + 1] = [1] * (len(example.service_intents) + 1)
        # to allow model predict "NONE"
        # intent_mask[MAX_NUM_INTENTS] = 1

        query_tokens = tokenizer.tokenize(example.question_text)

        if len(query_tokens) > max_query_length:
            query_tokens = query_tokens[0:max_query_length]

        tok_to_orig_index = []
        orig_to_tok_index = []
        all_doc_tokens = []
        for (i, token) in enumerate(example.doc_tokens):
            orig_to_tok_index.append(len(all_doc_tokens))
            if (token in special_task_tokens):
                sub_tokens = [token]
            else:
                sub_tokens = tokenizer.tokenize(token)
            for sub_token in sub_tokens:
                tok_to_orig_index.append(i)
                all_doc_tokens.append(sub_token)

        tok_start_position = None
        tok_end_position = None
        if is_training and example.is_impossible:
            tok_start_position = -1
            tok_end_position = -1
        if is_training and not example.is_impossible:
            tok_start_position = orig_to_tok_index[example.start_position]
            if example.end_position < len(example.doc_tokens) - 1:
                tok_end_position = orig_to_tok_index[example.end_position + 1] - 1
            else:
                tok_end_position = len(all_doc_tokens) - 1
            (tok_start_position, tok_end_position) = _improve_answer_span(
                all_doc_tokens, tok_start_position, tok_end_position, tokenizer,
                example.orig_answer_text)

        # The -3 accounts for [CLS], [SEP] and [SEP]
        max_tokens_for_doc = max_seq_length - len(query_tokens) - 3

        # We can have documents that are longer than the maximum sequence length.
        # To deal with this we do a sliding window approach, where we take chunks
        # of the up to our max length with a stride of `doc_stride`.
        _DocSpan = collections.namedtuple(  # pylint: disable=invalid-name
            "DocSpan", ["start", "length"])
        doc_spans = []
        start_offset = 0
        while start_offset < len(all_doc_tokens):
            length = len(all_doc_tokens) - start_offset
            if length > max_tokens_for_doc:
                length = max_tokens_for_doc
            doc_spans.append(_DocSpan(start=start_offset, length=length))
            if start_offset + length == len(all_doc_tokens):
                break
            start_offset += min(length, doc_stride)

        for (doc_span_index, doc_span) in enumerate(doc_spans):
            tokens = []
            token_to_orig_map = {}
            token_is_max_context = {}
            segment_ids = []

            # p_mask: mask with 1 for token than cannot be in the answer (0 for token which can be in an answer)
            # Original TF implem also keep the classification token (set to 0) (not sure why...)
            p_mask = []

            # CLS token at the beginning
            if not cls_token_at_end:
                tokens.append(cls_token)
                segment_ids.append(cls_token_segment_id)
                p_mask.append(0)
                cls_index = 0

            # Query
            for token in query_tokens:
                tokens.append(token)
                segment_ids.append(sequence_a_segment_id)
                p_mask.append(1)

            # SEP token
            tokens.append(sep_token)
            segment_ids.append(sequence_a_segment_id)
            p_mask.append(1)

            # Paragraph
            paragraph_len = min(max_hist_len, doc_span.length)
            for i in range(paragraph_len):
                split_token_index = doc_span.start + i
                token_to_orig_map[len(tokens)] = tok_to_orig_index[split_token_index]

                is_max_context = _check_is_max_context(doc_spans, doc_span_index,
                                                       split_token_index)
                token_is_max_context[len(tokens)] = is_max_context
                tokens.append(all_doc_tokens[split_token_index])
                segment_ids.append(sequence_b_segment_id)
                p_mask.append(0)

            # SEP token
            tokens.append(sep_token)
            segment_ids.append(sequence_b_segment_id)
            p_mask.append(1)

            input_ids = tokenizer.convert_tokens_to_ids(tokens)

            # The mask has 1 for real tokens and 0 for padding tokens. Only real
            # tokens are attended to.
            input_mask = [1 if mask_padding_with_zero else 0] * len(input_ids)
            old_len = len(tokens)
            counter = 0
            # Zero-pad up to the hist hist.
            while counter < max_hist_len - paragraph_len:
                input_ids.append(pad_token)
                input_mask.append(0 if mask_padding_with_zero else 1)
                segment_ids.append(pad_token_segment_id)
                p_mask.append(1)
                counter += 1

            old_len_with_pad = len(input_ids)

            int_coords = []
            counter = 0
            int_true_val_ind = MAX_NUM_INTENTS
            true_val = example.active_intent
            int_list = example.service_intents[:]
            int_list.append("NONE")
            if is_training:
                np.random.shuffle(int_list)
            for i, intent in enumerate(int_list):
                tokens.append(int_token)
                segment_ids.append(sequence_a_segment_id)
                # p_mask = 0, значит тут можно искать ответ для squad, нужен эксперимент
                p_mask.append(0)
                int_coords.append(old_len_with_pad + counter)
                counter += 1
                if (intent == true_val):
                    int_true_val_ind = i

                int_tokens = tokenizer.tokenize(intent)
                for tok in int_tokens:
                    tokens.append(tok)
                    segment_ids.append(sequence_a_segment_id)
                    p_mask.append(0)
                    counter += 1
            for i in range(MAX_NUM_INTENTS + 1 - len(int_list)):
                tokens.append(int_token)
                segment_ids.append(sequence_a_segment_id)
                p_mask.append(0)
                int_coords.append(old_len_with_pad + counter)
                counter += 1

            input_ids.extend(tokenizer.convert_tokens_to_ids(tokens[old_len:]))
            input_mask.extend([1 if mask_padding_with_zero else 0] * len(tokens[old_len:]))

            counter = 0
            # Zero-pad up to the sequence length.
            while len(tokens) - old_len + counter < max_intent_length:
                input_ids.append(pad_token)
                input_mask.append(0 if mask_padding_with_zero else 1)
                segment_ids.append(pad_token_segment_id)
                p_mask.append(1)
                counter += 1

            old_len = len(tokens)
            old_len_with_pad = len(input_ids)

            pv_coords = []
            counter = 0
            pv_true_val_ind = MAX_NUM_CAT_SLOT_VALUES
            if example.possible_values is not None:
                true_val = example.possible_values[example.value_id] if (example.value_id < len(example.possible_values)) else "None"
                pv_list = example.possible_values[:]
                pv_list.append("None")
                if (is_training):
                    np.random.shuffle(pv_list)
                for i, pv in enumerate(pv_list):
                    tokens.append(pv_token)
                    # segment_ids.append(pv_token_segment_id)
                    segment_ids.append(sequence_b_segment_id)
                    # p_mask = 0, значит тут можно искать ответ для squad, нужен эксперимент
                    p_mask.append(0)
                    pv_coords.append(old_len_with_pad + counter)
                    counter += 1
                    if (pv == true_val):
                        pv_true_val_ind = i

                    pos_val_tokens = tokenizer.tokenize(pv)
                    for tok in pos_val_tokens:
                        tokens.append(tok)
                        segment_ids.append(sequence_b_segment_id)
                        p_mask.append(0)
                        counter += 1
                for i in range(MAX_NUM_CAT_SLOT_VALUES + 1 - len(pv_list)):
                    tokens.append(pv_token)
                    segment_ids.append(sequence_b_segment_id)
                    p_mask.append(0)
                    pv_coords.append(old_len_with_pad + counter)
                    counter += 1
                # tokens.append('[unused2]')
                # segment_ids.append(sequence_b_segment_id)
                # p_mask.append(0)
                # pv_coords.append(old_len_with_pad + counter)

            # CLS token at the end
            if cls_token_at_end:
                tokens.append(cls_token)
                segment_ids.append(cls_token_segment_id)
                p_mask.append(0)
                cls_index = len(tokens) - 1  # Index of classification token


            input_ids.extend(tokenizer.convert_tokens_to_ids(tokens[old_len:]))
            input_mask.extend([1 if mask_padding_with_zero else 0] * len(tokens[old_len:]))

            # Zero-pad up to the sequence length.
            while len(input_ids) < max_seq_length:
                input_ids.append(pad_token)
                input_mask.append(0 if mask_padding_with_zero else 1)
                segment_ids.append(pad_token_segment_id)
                p_mask.append(1)

            pv_mask = np.zeros_like(input_mask)
            if example.possible_values is not None:
                for pv_coord in pv_coords:
                    pv_mask[pv_coord] = 1

            int_token_mask = np.zeros_like(input_mask)
            for int_coord in int_coords:
                int_token_mask[int_coord] = 1


            assert len(input_ids) == max_seq_length
            assert len(input_mask) == max_seq_length
            assert len(segment_ids) == max_seq_length

            span_is_impossible = example.is_impossible
            start_position = None
            end_position = None
            if is_training and not span_is_impossible:
                # For training, if our document chunk does not contain an annotation
                # we throw it out, since there is nothing to predict.
                doc_start = doc_span.start
                doc_end = doc_span.start + min(max_hist_len, doc_span.length) - 1
                out_of_span = False
                if not (tok_start_position >= doc_start and
                        tok_end_position <= doc_end):
                    out_of_span = True
                if out_of_span:
                    start_position = 0
                    end_position = 0
                    span_is_impossible = True
                else:
                    doc_offset = len(query_tokens) + 2
                    start_position = tok_start_position - doc_start + doc_offset
                    end_position = tok_end_position - doc_start + doc_offset

            if is_training and span_is_impossible:
                start_position = cls_index
                end_position = cls_index

            if example_index < 20:
                logger.info("*** Example ***")
                logger.info("unique_id: %s" % (unique_id))
                logger.info("example_index: %s" % (example_index))
                logger.info("doc_span_index: %s" % (doc_span_index))
                logger.info("tokens: %s" % " ".join(tokens))
                logger.info("token_to_orig_map: %s" % " ".join([
                    "%d:%d" % (x, y) for (x, y) in token_to_orig_map.items()]))
                logger.info("token_is_max_context: %s" % " ".join([
                    "%d:%s" % (x, y) for (x, y) in token_is_max_context.items()
                ]))
                logger.info("input_ids: %s" % " ".join([str(x) for x in input_ids]))
                logger.info(
                    "input_mask: %s" % " ".join([str(x) for x in input_mask]))
                logger.info(
                    "segment_ids: %s" % " ".join([str(x) for x in segment_ids]))
                if is_training and span_is_impossible:
                    logger.info("impossible example")
                if is_training and not span_is_impossible:
                    answer_text = " ".join(tokens[start_position:(end_position + 1)])
                    logger.info("start_position: %d" % (start_position))
                    logger.info("end_position: %d" % (end_position))
                    logger.info(
                        "answer: %s" % (answer_text))

            features.append(
                InputFeatures(
                    unique_id=unique_id,
                    example_index=example_index,
                    doc_span_index=doc_span_index,
                    tokens=tokens,
                    token_to_orig_map=token_to_orig_map,
                    token_is_max_context=token_is_max_context,
                    input_ids=input_ids,
                    input_mask=input_mask,
                    segment_ids=segment_ids,
                    cls_index=cls_index,
                    p_mask=p_mask,
                    paragraph_len=paragraph_len,
                    start_position=start_position,
                    end_position=end_position,
                    is_impossible=span_is_impossible,
                    turn_id=example.turn_id,
                    slot_name=example.slot_name,
                    service_name=example.service_name,
                    slot_status=example.slot_status,
                    is_categorical=example.is_categorical,
                    possible_values=example.possible_values,
                    value_id=example.value_id,
                    slot_mask=slot_mask,
                    is_requested=example.is_requested,
                    pv_mask=pv_mask,
                    pv_true_val=pv_true_val_ind,
                    service_intents=example.service_intents,
                    active_intent=example.active_intent,
                    active_intent_idx=int_true_val_ind,
                    intent_mask=intent_mask,
                    int_token_mask=int_token_mask
                ))
            unique_id += 1

    return features


def _improve_answer_span(doc_tokens, input_start, input_end, tokenizer,
                         orig_answer_text):
    """Returns tokenized answer spans that better match the annotated answer."""

    # The SQuAD annotations are character based. We first project them to
    # whitespace-tokenized words. But then after WordPiece tokenization, we can
    # often find a "better match". For example:
    #
    #   Question: What year was John Smith born?
    #   Context: The leader was John Smith (1895-1943).
    #   Answer: 1895
    #
    # The original whitespace-tokenized answer will be "(1895-1943).". However
    # after tokenization, our tokens will be "( 1895 - 1943 ) .". So we can match
    # the exact answer, 1895.
    #
    # However, this is not always possible. Consider the following:
    #
    #   Question: What country is the top exporter of electornics?
    #   Context: The Japanese electronics industry is the lagest in the world.
    #   Answer: Japan
    #
    # In this case, the annotator chose "Japan" as a character sub-span of
    # the word "Japanese". Since our WordPiece tokenizer does not split
    # "Japanese", we just use "Japanese" as the annotation. This is fairly rare
    # in SQuAD, but does happen.
    tok_answer_text = " ".join(tokenizer.tokenize(orig_answer_text))

    for new_start in range(input_start, input_end + 1):
        for new_end in range(input_end, new_start - 1, -1):
            text_span = " ".join(doc_tokens[new_start:(new_end + 1)])
            if text_span == tok_answer_text:
                return (new_start, new_end)

    return (input_start, input_end)


def _check_is_max_context(doc_spans, cur_span_index, position):
    """Check if this is the 'max context' doc span for the token."""

    # Because of the sliding window approach taken to scoring documents, a single
    # token can appear in multiple documents. E.g.
    #  Doc: the man went to the store and bought a gallon of milk
    #  Span A: the man went to the
    #  Span B: to the store and bought
    #  Span C: and bought a gallon of
    #  ...
    #
    # Now the word 'bought' will have two scores from spans B and C. We only
    # want to consider the score with "maximum context", which we define as
    # the *minimum* of its left and right context (the *sum* of left and
    # right context will always be the same, of course).
    #
    # In the example the maximum context for 'bought' would be span C since
    # it has 1 left context and 3 right context, while span B has 4 left context
    # and 0 right context.
    best_score = None
    best_span_index = None
    for (span_index, doc_span) in enumerate(doc_spans):
        end = doc_span.start + doc_span.length - 1
        if position < doc_span.start:
            continue
        if position > end:
            continue
        num_left_context = position - doc_span.start
        num_right_context = end - position
        score = min(num_left_context, num_right_context) + 0.01 * doc_span.length
        if best_score is None or score > best_score:
            best_score = score
            best_span_index = span_index

    return cur_span_index == best_span_index


RawResult = collections.namedtuple("RawResult",
                                   ["unique_id", "start_logits", "end_logits", "gate_logits",
                                    "cat_logits", "requested_logits", "intent_logits"])

def write_predictions(all_examples, all_features, all_results, n_best_size,
                      max_answer_length, do_lower_case, output_prediction_file,
                      output_intent_prediction_file, output_nbest_file,
                      output_null_log_odds_file, verbose_logging,
                      version_2_with_negative, null_score_diff_threshold):
    """Write final predictions to the json file and log-odds of null if needed."""

    example_index_to_features = collections.defaultdict(list)
    for feature in all_features:
        example_index_to_features[feature.example_index].append(feature)

    unique_id_to_result = {}
    for result in all_results:
        unique_id_to_result[result.unique_id] = result

    _PrelimPrediction = collections.namedtuple(  # pylint: disable=invalid-name
        "PrelimPrediction",
        ["feature_index", "start_index", "end_index", "start_logit", "end_logit",
         "slot_status", "cat_value", "requested_status", "active_intent"])

    all_predictions = collections.OrderedDict()
    all_nbest_json = collections.OrderedDict()
    scores_diff_json = collections.OrderedDict()

    preliminary_intent_predictions = collections.defaultdict(lambda: {'probs': [], 'intent_names': []})

    for (example_index, example) in tqdm(enumerate(all_examples), desc='Postprocessing predictions', total=len(all_examples)):
        features = example_index_to_features[example_index]

        prelim_predictions = []
        # keep track of the minimum score of null start+end of position 0
        score_null = 1000000  # large and positive
        min_null_feature_index = 0  # the paragraph slice with min null score
        null_start_logit = 0  # the start logit at the slice with min null score
        null_end_logit = 0  # the end logit at the slice with min null score
        for (feature_index, feature) in enumerate(features):
            result = unique_id_to_result[feature.unique_id]
            start_indexes = _get_best_indexes(result.start_logits, n_best_size)
            end_indexes = _get_best_indexes(result.end_logits, n_best_size)

            slot_status = IDX_TO_SLOT_STATUS_DICT[np.argmax(_compute_softmax(result.gate_logits))]
            requested_status = IDX_TO_REQUESTED_STATUS_DICT[np.argmax(_compute_softmax(result.requested_logits))]

            intent_probs = _compute_softmax(result.intent_logits)
            predicted_intent_idx = np.argmax(intent_probs)
            if predicted_intent_idx > len(example.service_intents) - 1:
                active_intent = "NONE"
            else:
                active_intent = example.service_intent_names[predicted_intent_idx]

            if example.is_categorical:
                correct_answer_idx = np.argmax(_compute_softmax(result.cat_logits))
                possible_values = example.possible_values
                if correct_answer_idx > len(possible_values) - 1:
                    cat_slot_value = "None"
                else:
                    cat_slot_value = str(possible_values[correct_answer_idx])
            else:
                cat_slot_value = "None"


            # if we could have irrelevant answers, get the min score of irrelevant
            if version_2_with_negative:
                feature_null_score = result.start_logits[0] + result.end_logits[0]
                if feature_null_score < score_null:
                    score_null = feature_null_score
                    min_null_feature_index = feature_index
                    null_start_logit = result.start_logits[0]
                    null_end_logit = result.end_logits[0]
            for start_index in start_indexes:
                for end_index in end_indexes:
                    # We could hypothetically create invalid predictions, e.g., predict
                    # that the start of the span is in the question. We throw out all
                    # invalid predictions.
                    if start_index >= len(feature.tokens):
                        continue
                    if end_index >= len(feature.tokens):
                        continue
                    if start_index not in feature.token_to_orig_map:
                        continue
                    if end_index not in feature.token_to_orig_map:
                        continue
                    if not feature.token_is_max_context.get(start_index, False):
                        continue
                    if end_index < start_index:
                        continue
                    length = end_index - start_index + 1
                    if length > max_answer_length:
                        continue
                    prelim_predictions.append(
                        _PrelimPrediction(
                            feature_index=feature_index,
                            start_index=start_index,
                            end_index=end_index,
                            start_logit=result.start_logits[start_index],
                            end_logit=result.end_logits[end_index],
                            slot_status=slot_status,
                            cat_value=cat_slot_value,
                            requested_status=requested_status,
                            active_intent=active_intent
                        )
                    )
        if version_2_with_negative:
            prelim_predictions.append(
                _PrelimPrediction(
                    feature_index=min_null_feature_index,
                    start_index=0,
                    end_index=0,
                    start_logit=null_start_logit,
                    end_logit=null_end_logit,
                    slot_status='none',
                    cat_value="None",
                    requested_status="not_requested",
                    active_intent="NONE"
                )
            )
        prelim_predictions = sorted(
            prelim_predictions,
            key=lambda x: (x.start_logit + x.end_logit),
            reverse=True)

        _NbestPrediction = collections.namedtuple(  # pylint: disable=invalid-name
            "NbestPrediction", ["text", "start_logit", "end_logit", "slot_status",
                                "cat_value", "requested_status", "active_intent"])

        seen_predictions = {}
        nbest = []

        if not example.is_categorical:
            for pred in prelim_predictions:
                if len(nbest) >= n_best_size:
                    break
                feature = features[pred.feature_index]
                if pred.start_index > 0:  # this is a non-null prediction
                    tok_tokens = feature.tokens[pred.start_index:(pred.end_index + 1)]
                    orig_doc_start = feature.token_to_orig_map[pred.start_index]
                    orig_doc_end = feature.token_to_orig_map[pred.end_index]
                    orig_tokens = example.doc_tokens[orig_doc_start:(orig_doc_end + 1)]
                    tok_text = " ".join(tok_tokens)

                    # De-tokenize WordPieces that have been split off.
                    tok_text = tok_text.replace(" ##", "")
                    tok_text = tok_text.replace("##", "")

                    # Clean whitespace
                    tok_text = tok_text.strip()
                    tok_text = " ".join(tok_text.split())
                    orig_text = " ".join(orig_tokens)

                    final_text = get_final_text(tok_text, orig_text, do_lower_case, verbose_logging)
                    if final_text in seen_predictions:
                        continue

                    seen_predictions[final_text] = True
                else:
                    final_text = ""
                    seen_predictions[final_text] = True

                nbest.append(
                    _NbestPrediction(
                        text=final_text,
                        start_logit=pred.start_logit,
                        end_logit=pred.end_logit,
                        slot_status=pred.slot_status,
                        cat_value=pred.cat_value,
                        requested_status=pred.requested_status,
                        active_intent=pred.active_intent
                    )
                )
            # if we didn't include the empty option in the n-best, include it
            if version_2_with_negative:
                if "" not in seen_predictions:
                    nbest.append(
                        _NbestPrediction(
                            text="",
                            start_logit=null_start_logit,
                            end_logit=null_end_logit,
                            slot_status="none",
                            cat_value="None",
                            requested_status="not_requested",
                            active_intent="NONE"
                        )
                    )

                # In very rare edge cases we could only have single null prediction.
                # So we just create a nonce prediction in this case to avoid failure.
                if len(nbest) == 1:
                    nbest.insert(0,
                        _NbestPrediction(text="", start_logit=0.0, end_logit=0.0, slot_status="none",
                                         cat_value="none", requested_status="not_requested", active_intent="NONE"))
        else:
            prelim_pred = prelim_predictions[0]
            nbest = [
                _NbestPrediction(
                    text="",
                    start_logit=0.0,
                    end_logit=0.0,
                    slot_status=prelim_pred.slot_status,
                    cat_value=prelim_pred.cat_value,
                    requested_status=prelim_pred.requested_status,
                    active_intent=prelim_pred.active_intent
                )
            ]

        # In very rare edge cases we could have no valid predictions. So we
        # just create a nonce prediction in this case to avoid failure.
        if not nbest:
            nbest.append(
                _NbestPrediction(text="", start_logit=0.0, end_logit=0.0,
                                 slot_status="none", cat_value="none",
                                 requested_status="not_requested", active_intent=active_intent))

        assert len(nbest) >= 1

        if not example.is_categorical:
            total_scores = []
            best_non_null_entry = None
            for entry in nbest:
                total_scores.append(entry.start_logit + entry.end_logit)
                if not best_non_null_entry:
                    if entry.text:
                        best_non_null_entry = entry
                        
            if best_non_null_entry is None:
                best_non_null_entry = nbest[0]

            probs = _compute_softmax(total_scores)
        else:
            best_non_null_entry = nbest[0]
            probs = [1]

        nbest_json = []
        for (i, entry) in enumerate(nbest):
            output = collections.OrderedDict()
            output["text"] = entry.text
            output["probability"] = probs[i]
            output["start_logit"] = entry.start_logit
            output["end_logit"] = entry.end_logit
            output["slot_status"] = entry.slot_status
            output["cat_value"] = entry.cat_value
            output["requested_status"] = entry.requested_status
            output["active_intent"] = entry.active_intent
            nbest_json.append(output)

        assert len(nbest_json) >= 1

        _, _, dialogue_id, turn_id = example.turn_id.split('-')
        service = example.service_name
        slot = example.slot_name
        prediction_coordinate = "-".join([dialogue_id, turn_id, service, slot])

        preliminary_intent_predictions["-".join([dialogue_id, turn_id, service])]['probs'].append(intent_probs)
        preliminary_intent_predictions["-".join([dialogue_id, turn_id, service])]['intent_names'] = example.service_intent_names

        if not example.is_categorical:
            if not version_2_with_negative:
                all_predictions[prediction_coordinate] = (
                    nbest_json[0]["text"], nbest_json[0]["slot_status"],
                    "None",
                    str(example.is_categorical),
                    nbest_json[0]["requested_status"],
                    nbest_json[0]["active_intent"]
                )
            else:
                # predict "" iff the null score - the score of best non-null > threshold
                score_diff = score_null - best_non_null_entry.start_logit - (
                    best_non_null_entry.end_logit)
                scores_diff_json[prediction_coordinate] = score_diff

                if score_diff > null_score_diff_threshold:
                    all_predictions[prediction_coordinate] = (
                        "",
                        "none",
                        "None",
                        str(example.is_categorical),
                        best_non_null_entry.requested_status,
                        best_non_null_entry.active_intent
                    )
                else:
                    all_predictions[prediction_coordinate] = (
                        best_non_null_entry.text,
                        best_non_null_entry.slot_status, "None",
                        str(example.is_categorical),
                        best_non_null_entry.requested_status,
                        best_non_null_entry.active_intent
                    )
            all_nbest_json[prediction_coordinate] = nbest_json
        else:
            all_predictions[prediction_coordinate] = (
                "",
                best_non_null_entry.slot_status,
                best_non_null_entry.cat_value,
                str(example.is_categorical),
                best_non_null_entry.requested_status,
                best_non_null_entry.active_intent
            )

    all_intent_predictions = {}
    for key, prelim_intent_prediction in preliminary_intent_predictions.items():
        max_probs = np.max(prelim_intent_prediction['probs'], axis=0)
        predicted_intent_idx = np.argmax(max_probs)
        intent_names = prelim_intent_prediction['intent_names']

        if predicted_intent_idx > len(intent_names) - 1:
            all_intent_predictions[key] = "NONE"
        else:
            all_intent_predictions[key] = intent_names[predicted_intent_idx]

    with open(output_prediction_file, "w") as writer:
        logger.info("Writing predictions to: %s" % output_prediction_file)
        writer.write(json.dumps(all_predictions, indent=4) + "\n")

    with open(output_intent_prediction_file, "w") as writer:
        logger.info("Writing intent predictions to: %s" % output_intent_prediction_file)
        writer.write(json.dumps(all_intent_predictions, indent=4) + "\n")

    with open(output_nbest_file, "w") as writer:
        logger.info("Writing nbest to: %s" % (output_nbest_file))
        writer.write(json.dumps(all_nbest_json, indent=4) + "\n")

    if version_2_with_negative:
        with open(output_null_log_odds_file, "w") as writer:
            writer.write(json.dumps(scores_diff_json, indent=4) + "\n")

    return all_predictions


# For XLNet (and XLM which uses the same head)
RawResultExtended = collections.namedtuple("RawResultExtended",
    ["unique_id", "start_top_log_probs", "start_top_index",
     "end_top_log_probs", "end_top_index", "cls_logits"])


def write_predictions_extended(all_examples, all_features, all_results, n_best_size,
                                max_answer_length, output_prediction_file,
                                output_nbest_file,
                                output_null_log_odds_file, orig_data_file,
                                start_n_top, end_n_top, version_2_with_negative,
                                tokenizer, verbose_logging):
    """ XLNet write prediction logic (more complex than Bert's).
        Write final predictions to the json file and log-odds of null if needed.

        Requires utils_squad_evaluate.py
    """
    _PrelimPrediction = collections.namedtuple(  # pylint: disable=invalid-name
        "PrelimPrediction",
        ["feature_index", "start_index", "end_index",
        "start_log_prob", "end_log_prob"])

    _NbestPrediction = collections.namedtuple(  # pylint: disable=invalid-name
        "NbestPrediction", ["text", "start_log_prob", "end_log_prob"])

    logger.info("Writing predictions to: %s", output_prediction_file)
    # logger.info("Writing nbest to: %s" % (output_nbest_file))

    example_index_to_features = collections.defaultdict(list)
    for feature in all_features:
        example_index_to_features[feature.example_index].append(feature)

    unique_id_to_result = {}
    for result in all_results:
        unique_id_to_result[result.unique_id] = result

    all_predictions = collections.OrderedDict()
    all_nbest_json = collections.OrderedDict()
    scores_diff_json = collections.OrderedDict()

    for (example_index, example) in enumerate(all_examples):
        features = example_index_to_features[example_index]

        prelim_predictions = []
        # keep track of the minimum score of null start+end of position 0
        score_null = 1000000  # large and positive

        for (feature_index, feature) in enumerate(features):
            result = unique_id_to_result[feature.unique_id]

            cur_null_score = result.cls_logits

            # if we could have irrelevant answers, get the min score of irrelevant
            score_null = min(score_null, cur_null_score)

            for i in range(start_n_top):
                for j in range(end_n_top):
                    start_log_prob = result.start_top_log_probs[i]
                    start_index = result.start_top_index[i]

                    j_index = i * end_n_top + j

                    end_log_prob = result.end_top_log_probs[j_index]
                    end_index = result.end_top_index[j_index]

                    # We could hypothetically create invalid predictions, e.g., predict
                    # that the start of the span is in the question. We throw out all
                    # invalid predictions.
                    if start_index >= feature.paragraph_len - 1:
                        continue
                    if end_index >= feature.paragraph_len - 1:
                        continue

                    if not feature.token_is_max_context.get(start_index, False):
                        continue
                    if end_index < start_index:
                        continue
                    length = end_index - start_index + 1
                    if length > max_answer_length:
                        continue

                    prelim_predictions.append(
                        _PrelimPrediction(
                            feature_index=feature_index,
                            start_index=start_index,
                            end_index=end_index,
                            start_log_prob=start_log_prob,
                            end_log_prob=end_log_prob))

        prelim_predictions = sorted(
            prelim_predictions,
            key=lambda x: (x.start_log_prob + x.end_log_prob),
            reverse=True)

        seen_predictions = {}
        nbest = []
        for pred in prelim_predictions:
            if len(nbest) >= n_best_size:
                break
            feature = features[pred.feature_index]

            # XLNet un-tokenizer
            # Let's keep it simple for now and see if we need all this later.
            # 
            # tok_start_to_orig_index = feature.tok_start_to_orig_index
            # tok_end_to_orig_index = feature.tok_end_to_orig_index
            # start_orig_pos = tok_start_to_orig_index[pred.start_index]
            # end_orig_pos = tok_end_to_orig_index[pred.end_index]
            # paragraph_text = example.paragraph_text
            # final_text = paragraph_text[start_orig_pos: end_orig_pos + 1].strip()

            # Previously used Bert untokenizer
            tok_tokens = feature.tokens[pred.start_index:(pred.end_index + 1)]
            orig_doc_start = feature.token_to_orig_map[pred.start_index]
            orig_doc_end = feature.token_to_orig_map[pred.end_index]
            orig_tokens = example.doc_tokens[orig_doc_start:(orig_doc_end + 1)]
            tok_text = tokenizer.convert_tokens_to_string(tok_tokens)

            # Clean whitespace
            tok_text = tok_text.strip()
            tok_text = " ".join(tok_text.split())
            orig_text = " ".join(orig_tokens)

            final_text = get_final_text(tok_text, orig_text, tokenizer.do_lower_case,
                                        verbose_logging)

            if final_text in seen_predictions:
                continue

            seen_predictions[final_text] = True

            nbest.append(
                _NbestPrediction(
                    text=final_text,
                    start_log_prob=pred.start_log_prob,
                    end_log_prob=pred.end_log_prob))

        # In very rare edge cases we could have no valid predictions. So we
        # just create a nonce prediction in this case to avoid failure.
        if not nbest:
            nbest.append(
                _NbestPrediction(text="", start_log_prob=-1e6,
                end_log_prob=-1e6))

        total_scores = []
        best_non_null_entry = None
        for entry in nbest:
            total_scores.append(entry.start_log_prob + entry.end_log_prob)
            if not best_non_null_entry:
                best_non_null_entry = entry

        probs = _compute_softmax(total_scores)

        nbest_json = []
        for (i, entry) in enumerate(nbest):
            output = collections.OrderedDict()
            output["text"] = entry.text
            output["probability"] = probs[i]
            output["start_log_prob"] = entry.start_log_prob
            output["end_log_prob"] = entry.end_log_prob
            nbest_json.append(output)

        assert len(nbest_json) >= 1
        assert best_non_null_entry is not None

        score_diff = score_null
        scores_diff_json[example.qas_id] = score_diff
        # note(zhiliny): always predict best_non_null_entry
        # and the evaluation script will search for the best threshold
        all_predictions[example.qas_id] = best_non_null_entry.text

        all_nbest_json[example.qas_id] = nbest_json

    with open(output_prediction_file, "w") as writer:
        writer.write(json.dumps(all_predictions, indent=4) + "\n")

    with open(output_nbest_file, "w") as writer:
        writer.write(json.dumps(all_nbest_json, indent=4) + "\n")

    if version_2_with_negative:
        with open(output_null_log_odds_file, "w") as writer:
            writer.write(json.dumps(scores_diff_json, indent=4) + "\n")

    with open(orig_data_file, "r", encoding='utf-8') as reader:
        orig_data = json.load(reader)["data"]

    qid_to_has_ans = make_qid_to_has_ans(orig_data)
    has_ans_qids = [k for k, v in qid_to_has_ans.items() if v]
    no_ans_qids = [k for k, v in qid_to_has_ans.items() if not v]
    exact_raw, f1_raw = get_raw_scores(orig_data, all_predictions)
    out_eval = {}

    find_all_best_thresh_v2(out_eval, all_predictions, exact_raw, f1_raw, scores_diff_json, qid_to_has_ans)

    return out_eval


def get_final_text(pred_text, orig_text, do_lower_case, verbose_logging=False):
    """Project the tokenized prediction back to the original text."""

    # When we created the data, we kept track of the alignment between original
    # (whitespace tokenized) tokens and our WordPiece tokenized tokens. So
    # now `orig_text` contains the span of our original text corresponding to the
    # span that we predicted.
    #
    # However, `orig_text` may contain extra characters that we don't want in
    # our prediction.
    #
    # For example, let's say:
    #   pred_text = steve smith
    #   orig_text = Steve Smith's
    #
    # We don't want to return `orig_text` because it contains the extra "'s".
    #
    # We don't want to return `pred_text` because it's already been normalized
    # (the SQuAD eval script also does punctuation stripping/lower casing but
    # our tokenizer does additional normalization like stripping accent
    # characters).
    #
    # What we really want to return is "Steve Smith".
    #
    # Therefore, we have to apply a semi-complicated alignment heuristic between
    # `pred_text` and `orig_text` to get a character-to-character alignment. This
    # can fail in certain cases in which case we just return `orig_text`.

    def _strip_spaces(text):
        ns_chars = []
        ns_to_s_map = collections.OrderedDict()
        for (i, c) in enumerate(text):
            if c == " ":
                continue
            ns_to_s_map[len(ns_chars)] = i
            ns_chars.append(c)
        ns_text = "".join(ns_chars)
        return (ns_text, ns_to_s_map)

    # We first tokenize `orig_text`, strip whitespace from the result
    # and `pred_text`, and check if they are the same length. If they are
    # NOT the same length, the heuristic has failed. If they are the same
    # length, we assume the characters are one-to-one aligned.
    tokenizer = BasicTokenizer(do_lower_case=do_lower_case)

    tok_text = " ".join(tokenizer.tokenize(orig_text))

    start_position = tok_text.find(pred_text)
    if start_position == -1:
        if verbose_logging:
            logger.info(
                "Unable to find text: '%s' in '%s'" % (pred_text, orig_text))
        return orig_text
    end_position = start_position + len(pred_text) - 1

    (orig_ns_text, orig_ns_to_s_map) = _strip_spaces(orig_text)
    (tok_ns_text, tok_ns_to_s_map) = _strip_spaces(tok_text)

    if len(orig_ns_text) != len(tok_ns_text):
        if verbose_logging:
            logger.info("Length not equal after stripping spaces: '%s' vs '%s'",
                        orig_ns_text, tok_ns_text)
        return orig_text

    # We then project the characters in `pred_text` back to `orig_text` using
    # the character-to-character alignment.
    tok_s_to_ns_map = {}
    for (i, tok_index) in tok_ns_to_s_map.items():
        tok_s_to_ns_map[tok_index] = i

    orig_start_position = None
    if start_position in tok_s_to_ns_map:
        ns_start_position = tok_s_to_ns_map[start_position]
        if ns_start_position in orig_ns_to_s_map:
            orig_start_position = orig_ns_to_s_map[ns_start_position]

    if orig_start_position is None:
        if verbose_logging:
            logger.info("Couldn't map start position")
        return orig_text

    orig_end_position = None
    if end_position in tok_s_to_ns_map:
        ns_end_position = tok_s_to_ns_map[end_position]
        if ns_end_position in orig_ns_to_s_map:
            orig_end_position = orig_ns_to_s_map[ns_end_position]

    if orig_end_position is None:
        if verbose_logging:
            logger.info("Couldn't map end position")
        return orig_text

    output_text = orig_text[orig_start_position:(orig_end_position + 1)]
    return output_text


def _get_best_indexes(logits, n_best_size):
    """Get the n-best logits from a list."""
    index_and_score = sorted(enumerate(logits), key=lambda x: x[1], reverse=True)

    best_indexes = []
    for i in range(len(index_and_score)):
        if i >= n_best_size:
            break
        best_indexes.append(index_and_score[i][0])
    return best_indexes


def _compute_softmax(scores):
    """Compute softmax probability over raw logits."""
    if not scores:
        return []

    max_score = None
    for score in scores:
        if max_score is None or score > max_score:
            max_score = score

    exp_scores = []
    total_sum = 0.0
    for score in scores:
        x = math.exp(score - max_score)
        exp_scores.append(x)
        total_sum += x

    probs = []
    for score in exp_scores:
        probs.append(score / total_sum)
    return probs
